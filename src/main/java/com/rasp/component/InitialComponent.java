package com.rasp.component;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import com.rasp.entity.Light;

@ApplicationScope
@Component
public class InitialComponent {

	private Light light;

	public InitialComponent() {
	}

	public Light getLight() {
		return light;
	}

	public void setLight(Light light) {
		this.light = light;
	}

}
