package com.rasp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rasp.entity.Light;
import com.rasp.service.LightService;

@RestController
public class ApiRest {

	@Autowired
	private LightService lightService;

	@GetMapping("/turnOn")
	public ResponseEntity<Light> showLightState() {
		Light light = new Light();
		light.setName("Comedor");
		light.setState(false);
		lightService.turnOn(light);
		ResponseEntity<Light> response = new ResponseEntity<Light>(light, HttpStatus.OK);

		return response;
	}

}
