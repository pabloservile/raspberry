package com.rasp.entity;

public class Light {

	private String name;
	private boolean state;

	public Light() {
	}

	public Light(String name, boolean state) {
		this.name = name;
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

}
