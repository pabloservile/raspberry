package com.rasp.service;

import com.rasp.entity.Light;

public interface LightService {

	public abstract void turnOn(Light light);

	public abstract void turnOff(Light light);

}
