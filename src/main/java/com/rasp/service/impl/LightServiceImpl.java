package com.rasp.service.impl;

import org.springframework.stereotype.Service;

import com.rasp.entity.Light;
import com.rasp.service.LightService;

@Service
public class LightServiceImpl implements LightService {

	@Override
	public void turnOn(Light light) {
		light.setState(true);
	}

	@Override
	public void turnOff(Light light) {
		light.setState(false);
	}

}
